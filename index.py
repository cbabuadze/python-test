from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/hi")
def hello():
    return "Hi Hi!!!"

@app.route("/hhola")
def hello():
    return "Hihola!"

@app.route("/new")
def new():
    return "new"

if __name__ == "__main__":
    app.run(host='0.0.0.0')
